import time
startTime = time.time()

import discord
from datetime import datetime

with open('token.txt', 'r') as f:
    token = f.readline()

client = discord.Client()

@client.event
async def on_ready():
    startupTime = time.time()-startTime
    print('Logged in as {1} ({0:.3f}s)'.format(startupTime, client.user))

@client.event
async def on_message(message):
    print([message])
    print([message.content])
    if message.author == client.user:
        return

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')

client.run(token)